function ApplicationWindow() {
	//declare module dependencies
	var MasterView = require('ui/common/MasterView'),
		DetailView = require('ui/common/DetailView');

	//create object instance
	var self = Ti.UI.createWindow({
		exitOnClose:true,
		navBarHidden:false,
		backgroundColor:'#ffffff'
	});

	//construct UI
	var masterView = MasterView();
	self.add(masterView);


	return self;
};

module.exports = ApplicationWindow;
