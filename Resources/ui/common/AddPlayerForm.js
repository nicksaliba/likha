var db = Ti.Database.open('likha');

function AddPlayerForm() {
	var self = Ti.UI.createWindow({
		backgroundColor : '#FFF'
	});

	var nameLabel = Ti.UI.createLabel({
		text : "Name: ",
		top : '10dp',
		left : '10dp'
	});

	var nameField = Ti.UI.createTextField({
		width : '100dp',
		height : '50dp',
		top : '10dp',
		left : '100dp'
	});

	var addButton = Ti.UI.createButton({
		title : "Add Player",
		top : 30
	});

	addButton.addEventListener('click', function(e) {
		db.execute("INSERT INTO players(name) VALUES(?)", nameField.value);
	});

	var holder = Ti.UI.createView();
	holder.add(nameLabel);
	holder.add(nameField);

	self.add(holder);
	self.add(addButton);

	self.addEventListener('open', function(e) {
		db.execute('CREATE TABLE IF NOT EXISTS players(name varchar(255))');
		var rows = db.execute('SELECT name FROM players');
		var players = [];
		while (rows.isValidRow()) {
			players.push(rows.fieldByName('name'));
			rows.next();
		}
		var table = Ti.UI.createTableView({
			data : players,
			backgroundColor: 'red',
			color: 'blue'
		});
		if (players.length) {
			table.top = '50dp';
			table.left = '30dp';
			self.add(table);
		}
	});

	return self;
}

module.exports = AddPlayerForm;
