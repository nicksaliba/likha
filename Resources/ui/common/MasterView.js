var AddPlayerForm = require('ui/common/AddPlayerForm');

//Master View Component Constructor
function MasterView() {
	//create object instance, parasitic subclass of Observable
	var self = Ti.UI.createView({
		backgroundColor:'white'
	});

	var addPalyerButton = Ti.UI.createButton({
		title: "Add Player",
		height: '44dp'
	});
	
	addPalyerButton.addEventListener('click', function(e){
		var addPlayerForm = AddPlayerForm();
		addPlayerForm.open();
	});
	
	self.add(addPalyerButton);

	return self;
};

module.exports = MasterView;